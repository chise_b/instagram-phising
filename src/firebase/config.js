import firebase from "firebase";

// For Firebase JS SDK v.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyDZT1swxKTR6L4-P8eIA-X7HU6puyotimo",
    authDomain: "fake-instagram-96700.firebaseapp.com",
    databaseURL: "https://fake-instagram-96700-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "fake-instagram-96700",
    storageBucket: "fake-instagram-96700.appspot.com",
    messagingSenderId: "433243681295",
    appId: "1:433243681295:web:860259710756457cffa97a",
    measurementId: "G-M8PSC3G89S"
};

// if you think about using is with .env file
//
// const firebaseConfig = {
//   apiKey: process.env.REACT_APP_API_KEY,
//   authDomain: process.env.REACT_APP_AUTH_DOMAIN,
//   projectId: process.env.REACT_APP_PROJECT_ID,
//   storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
//   messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
//   appId: process.env.REACT_APP_APP_ID,
// };

const firebaseApp = firebase.initializeApp(firebaseConfig);

export const auth = firebaseApp.auth();
export const storage = firebaseApp.storage();
export const db = firebaseApp.firestore();
